<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psy\Exception\ErrorException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    private $guard;

    public function __construct()
    {

        $this->guard = \Auth::guard('api');
    }


    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $user = User::where('email', $credentials['email'])->first();
        if (!$user || !Auth::attempt($credentials)) {
            return response()->json(['message' => __('messages.invalid_credentials')], 401);
        }

        $token = JWTAuth::fromUser($user);
        return response()->json(compact('token'));
    }

    public function logout()
    {
        try{
            $this->guard->logout();
            return response()->json(['message' => 'Successfully logged out']);
        } catch (ErrorException $e) {
            return response()->json(['status' => 0]);
        }

    }
}
