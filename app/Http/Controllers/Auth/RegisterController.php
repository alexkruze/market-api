<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'full_name' => 'required|string|max:255'
        ]);
        if (count($validator->errors())) {
            return response()->json($validator->errors(), 400);
        }

        $data = $request->all();
        return response()->json(User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'full_name' => $data['full_name'],
            'password' => bcrypt($data['password']),
            'role' =>'customer'
        ]), 201);
    }
}
