<?php

namespace App\Http\Controllers\Category;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|unique:categories',
            'category_id' => 'numeric|exists:categories,id'
        ]);

        if (count($validator->errors())) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        Category::create($request->all());

        return response()->json(['status' => 1], 201);
    }

    public function delete($id)
    {
        if (auth()->user()->role !== 'admin') return abort(403);

        $category = Category::findOrFail($id);
        $category->delete();
        return response()->json(['status' => 1], 202);
    }

    public function listCategories()
    {
        $per_page = 20;
        return response()->json(['status' => 1, 'categories' => Category::where('category_id', null)->orderby('name', 'ASC')->paginate($per_page)], 200);
    }

    public function filterListCategories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'q' => 'string|min:3',
            'category_id' => 'integer|exists:categories,id'
        ]);

        if (count($validator->errors())) {
            return response()->json(['status' => 0, 'errors' => $validator->errors()], 400);
        }

        $q = $request->input('q');
        $category_id = $request->input('category_id');
        $per_page = 20;

        $query = Category::query();

        if ($category_id) {
            $query->find($category_id);
        }

        if ($q) {
            $query->where('name', 'like', '%' . $q . '%');
        }

        return response()->json(['status' => 1, 'categories' => $query->paginate($per_page)], 200);
    }

    public function getCategory(Category $category)
    {
        if (!$category->parent) {
            return response()->json(['status' => 1, 'category' => $category->load('children')->toArray()], 200);
        } else {
            return response()->json(['status' => 1, 'category' => $category->load('allChildren')->toArray()], 200);
        }
//        return response()->json(['status' => 1, 'categories' => $query->paginate($per_page)], 200);
    }
}
