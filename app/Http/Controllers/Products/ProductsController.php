<?php

namespace App\Http\Controllers\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Psy\Exception\ErrorException;

class ProductsController extends Controller
{
    public function create(Request $request)
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|min:6',
            'description' => 'required|string|min:100',
            'category_id' => 'required|integer|exists:categories,id',
            'price' => array('required', 'regex:' . $regex),
            'user_shops_id' => 'required|integer|exists:user_shops,id',
            'images' => 'required|array',
            'images.*.name' => 'required'
        ],
        [
            'input.regex' => 'Float num only'
        ]);

        if (count($validator->errors())) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        if (!auth()->user()->shops()->find($request->input('user_shops_id'))) {
            return response()->json(['status' => 0], 403);
        }

        try{
            $product = Product::create($request->only(['title',
                                                            'description',
                                                            'category_id',
                                                            'price',
                                                            'user_shops_id'
                                                        ]));
            $product_slug = str_slug($product->title);
            foreach ($request->input('images') as $img) {
                Storage::move('public/tmp/'.$img['name'],
                                'public/product/' . $product->id . '/' . $product_slug . '_' . $img['name']);
                Storage::move('public/tmp/thumb_'.$img['name'],
                                'public/product/' . $product->id . '/thumb_' . $product_slug . '_' . $img['name']);
            }
            return response()->json(['status' => 1], 201);
        } catch (ErrorException $e) {
            return response()->json(['status' => 0], 406);
        }
    }

    public function update($id, Request $request)
    {
        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";
        $validator = Validator::make($request->all(), [
            'title' => 'string|min:6',
            'description' => 'string|min:100',
            'category_id' => 'integer|exists:categories,id',
            'price' => array('regex:' . $regex),
            'images' => 'required|array',
            'images.*.name' => 'required'
        ],
            [
                'input.regex' => 'Float num only'
            ]
        );

        if (count($validator->errors())) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $product = Product::findOrFail($id);

        if (!$product->userShop->users()->find(auth()->id())) {
            return response()->json(['status' => 0], 403);
        }

        $product->update($request->only(['title',
            'description',
            'category_id',
            'price'
        ]));

        $product_images = $product->images['photos'];
        $images_tmp = array();

        foreach ($request->input('images') as $img) {
            array_push($images_tmp, env('APP_URL') . '/storage/product/' . $product->id . '/' . $img['name']);
        }

        $need_delete = array_diff($product_images, $images_tmp);
        $need_load = array_diff($images_tmp, $product_images);

        if (count($need_delete)) {
            foreach ($need_delete as $item) {
                $file_name = explode('/', $item);
                $file_name = array_pop($file_name);
                Storage::delete('public/product/' . $product->id . '/' . $file_name);
                Storage::delete('public/product/' . $product->id . '/thumb_' . $file_name);
            }
        }

        if (count($need_load)) {
            foreach ($need_load as $item) {
                $file_name = explode('/', $item);
                $file_name = array_pop($file_name);
                Storage::move('public/tmp/' . $file_name,
                    'public/product/' . $product->id . '/' . str_slug($product->title) . '_' . $file_name);
                Storage::move('public/tmp/thumb_' . $file_name,
                    'public/product/' . $product->id . '/thumb_' . str_slug($product->title) . '_' . $file_name);
            }
        }
        return response()->json(['status' => 1], 202);
    }

    public function listProducts()
    {
        $per_page = 10;
        return Product::paginate($per_page);
    }

    public function loadImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image'
        ]);
        if (count($validator->errors())) {
            return response()->json(['errors' => $validator->errors()], 400);
        }
        try {
            $file = $request->file('image');
            $file_name = sha1_file($file) . $file->getCTime() . '.' . $file->getClientOriginalExtension();
            Storage::putFileAs('public/tmp', $file, $file_name);

            $img = Image::make(Input::file('image'));
            $img->resize(240, 240, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->encode($file->getClientOriginalExtension());

            Storage::put('public/tmp/thumb_' . $file_name, $img->__toString());
            return response()->json(['image' => env('APP_URL') . '/storage/tmp/thumb_' . $file_name, 'name' => $file_name], 201);
        } catch (ErrorException $e) {
            return response()->json(['status' => 0], 400);
        }
    }
}
