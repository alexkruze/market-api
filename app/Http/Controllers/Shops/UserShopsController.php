<?php

namespace App\Http\Controllers\Shops;

use App\Http\Resources\Shops\UserShopCollection;
use App\Models\UserShop;
use App\Models\UserShopContact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Psy\Exception\ErrorException;
use App\Http\Resources\Shops\UserShop as UserShopResources;

class UserShopsController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shop_name' => 'required|min:6',
            'contacts' => 'required|array',
            'contacts.*.title' => 'required|min:3',
            'contacts.*.value' => 'required|min:3'
        ]);
        if(count($validator->errors())) {
            return response()->json(['status' => 0, 'errors' => $validator->errors()], 400);
        }
        $shop = UserShop::create(
                            ['shop_name' => $request->input('shop_name'),
                                'slug' => $request->input('shop_name')]);

        $shop->shopUsers()->create(['user_id' => auth()->id(), 'role' => 'admin']);
        $shop->contacts()->createMany($request->input('contacts'));
        return response()->json(['status' => 1], 201);
    }

    public function delete($id)
    {
        $shop = UserShop::with('users')->where('id', $id)->whereHas('shopUsers', function ($q) {
            $q->where(['user_id' => auth()->id(), 'role' => 'admin']);
        })->firstOrFail();
        $shop->delete();
        return response()->json(['status' => 1], 200);
    }

    public function listShops()
    {
        $per_page = 5;
        $shops = UserShop::with('users', 'contacts')->paginate(2);
        $response = new UserShopCollection($shops);
        return $response;
    }
}
