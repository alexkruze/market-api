<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;
use Psy\Exception\ErrorException;
use App\Http\Resources\User\User as UserResources;

class UserController extends Controller
{
    public function listUsers()
    {
        return response()->json(['status' => 1, 'users' => UserResources::collection(User::all())], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'string|email|max:255|unique:users,email',
            'full_name' => 'string|min:3',
            'name' => 'string|min:3',
            'image' => 'array',
            'image.name' => 'string'
        ]);

        if (count($validator->errors())) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user = User::findOrFail(auth()->id());

        $user->update($request->only([
                        'email',
                        'full_name',
                        'name'
                    ]));
        $avatar = $request->input('image');
        if ($avatar && !Storage::disk('public')->exists('user/' . $avatar['name'])) {
            Storage::move('public/tmp/' . $avatar['name'],
                'public/user/' . $user->id . '/' . str_slug($user->name) . $avatar['name']);
        }

        return response()->json(['status' => 1], 202);
    }

    public function loadImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image'
        ]);
        if (count($validator->errors())) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        try {
            $file = $request->file('image');
            $file_name = sha1_file($file) . $file->getCTime() . '.' . $file->getClientOriginalExtension();

            $img = Image::make(Input::file('image'));
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->encode($file->getClientOriginalExtension());

            Storage::put('public/tmp/' . $file_name, $img->__toString());

            return response()->json(['status' => 1, 'image' => env('APP_URL') . '/storage/tmp/' . $file_name, 'name' => $file_name], 201);
        } catch (ErrorException $e) {
            return response()->json(['status' => 0], 400);
        }
    }

    public function getUser()
    {
        return response()->json(['status' => 1, 'user' => new UserResources(auth()->user())], 200);
    }

    public function createFromAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3',
            'full_name' => 'required|string|min:3',
            'email' => 'required|string|unique:users,email',
            'role' => [
                        'required',
                        Rule::in(['admin', 'moderator', 'seller', 'customer'])
                ]
        ]);
        if (count($validator->errors())) {
            return response()->json(['status' => 0, 'errors' => $validator->errors()], 400);
        }

        $data = $request->all();
        $data['password'] = bcrypt('123456');

        try {
            User::create($data);
            return response()->json(['status' => 1], 202);
        } catch (ErrorException $e) {
            return response()->json(['status' => 0], 500);
        }
    }

}
