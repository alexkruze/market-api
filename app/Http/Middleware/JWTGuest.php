<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWTGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$token = JWTAuth::getToken()) {
            return $next($request);
        }

        try {
            $user = JWTAuth::authenticate($token);
        } catch (TokenExpiredException $e) {
            return response()->json(['message' => 'token_expired'], 403);
        } catch (JWTException $e) {
            return response()->json(['message' => 'token_invalid'], 401);
        } if (!$user) {
        throw (new ModelNotFoundException)->setModel('App\\Models\\User');
    }

        return $next($request);
    }
}
