<?php

namespace App\Http\Resources\Shops;

use App\Http\Resources\User\User;
use Illuminate\Http\Resources\Json\Resource;

class UserShop extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->shop_name,
            'slug' => $this->slug,
            'users' => User::collection($this->whenLoaded('users')),
            'contacts' => UserShopContact::collection($this->whenLoaded('contacts'))
        ];
    }
}
