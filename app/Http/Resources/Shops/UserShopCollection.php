<?php

namespace App\Http\Resources\Shops;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserShopCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
    }

    public function with($request)
    {
        return [
            'status' => 1
        ];
    }
}
