<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Shops\UserShop;
use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'shops' => UserShop::collection($this->whenLoaded('shops'))
        ];
    }
}
