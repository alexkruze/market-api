<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'category_id'
    ];


    public function children()
    {
        return $this->hasMany(self::class, 'category_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class,'category_id', 'id');
    }

    public function parentList()
    {
        return $this->parent()->with('parentList');
    }

    public function allChildren()
    {
        return $this->children()->with('allChildren');
    }
}
