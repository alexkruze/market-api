<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    protected $fillable = [
        'id',
        'title',
        'description',
        'category_id',
        'price',
        'count',
        'user_shops_id',
    ];

    protected $appends = ['images', 'default_image'];

    public function userShop()
    {
        return $this->hasOne(UserShop::class, 'id', 'user_shops_id');
    }

    public function getImagesAttribute()
    {
        $path = snake_case(class_basename($this)) . "/$this->id";

        if (!Storage::disk('public')->exists($path)) {
            return null;
        }
        $files = Storage::disk('public')->allFiles($path);

        $array_images = array();
        $array_images['thumbs'] = array();
        $array_images['photos'] = array();
        foreach ($files as $file) {
            $tmp_name = explode('/',$file);
            $tmp_file = array_pop($tmp_name);
            if (strpos($tmp_file, 'thumb_') !== false) {
                $file_path = env('APP_URL') . '/storage/' . implode('/', $tmp_name) . '/' . $tmp_file;
                array_push($array_images['thumbs'], $file_path);
            } else {
                $file_path = env('APP_URL') . '/storage/' . implode('/', $tmp_name) . '/' . $tmp_file;
                array_push($array_images['photos'], $file_path);
            }
        }

        return $array_images;
    }

    public function getDefaultImageAttribute()
    {
        $path = snake_case(class_basename($this)) . "/$this->id";

        if (!Storage::disk('public')->exists($path)) {
            return null;
        }
        $files = Storage::disk('public')->allFiles($path);

        foreach ($files as $file) {
            $tmp_name = explode('/',$file);
            $tmp_file = array_pop($tmp_name);
            if (strpos($tmp_file, 'thumb_') !== false) {
                $file_path = env('APP_URL') . '/storage/' . implode('/', $tmp_name) . '/' . $tmp_file;
                return $file_path;
            }
        }
        return null;
    }
}
