<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'full_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['avatar'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function shops()
    {
        return $this->belongsToMany(UserShop::class, 'user_shop_users', 'user_id', 'user_shops_id');
    }

    public function getAvatarAttribute()
    {
        $path = snake_case(class_basename($this)) . "/$this->id";

        if (!Storage::disk('public')->exists($path)) {
            return null;
        }
        $files = Storage::disk('public')->allFiles($path);
        if (!count($files)) {
            return null;
        }
        return env('APP_URL') . '/storage/' . $files[0];
    }
}
