<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserShop extends Model
{
    protected $fillable = [
        'id',
        'shop_name',
        'slug'
    ];

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    public function contacts()
    {
        return $this->hasMany(UserShopContact::class, 'user_shops_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_shop_users', 'user_shops_id', 'user_id');
    }

    public function shopUsers()
    {
        return $this->hasMany(UserShopUser::class, 'user_shops_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'user_shops_id', 'id');
    }
}
