<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserShopContact extends Model
{
    protected $fillable = [
        'user_shops_id',
        'title',
        'value'
    ];
}
