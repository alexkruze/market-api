<?php

use App\Models\UserShop;
use Faker\Generator as Faker;

$factory->define(UserShop::class, function (Faker $faker) {
    return [
        'shop_name' => $faker->name,
        'contacts' => [
            [
                'title' => $faker->domainWord,
                'value' => $faker->e164PhoneNumber
            ]
        ]
    ];
});
