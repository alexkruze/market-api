<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $categories = json_decode(Storage::disk('public')->get('tmp/categories.json'));
        foreach ($categories->categories as $category) {
            Category::create([
                'id' => $category[1],
                'name' => $category[2],
                'slug' => str_slug($category[2])
            ]);
        }
        foreach ($categories->categories as $category) {
            if (Category::find($category[4])) {
                $category_parent = Category::find($category[1]);
                $category_parent->category_id = $category[4] ?: null;
                $category_parent->save();
            }
        }
    }
}
