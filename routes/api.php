<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->post('/login', 'Auth\LoginController@login');
Route::middleware('api')->post('/logout', 'Auth\LoginController@logout');
Route::middleware('api')->post('/registration', 'Auth\RegisterController@create');
Route::middleware('api-user')->post('/test', 'HomeController@test');

Route::group(['prefix' => 'users', 'middleware' => 'api', 'namespace' => 'User'], function () {
    Route::get('/', 'UserController@listUsers');
    Route::middleware('api-user')->patch('/', 'UserController@update');
    Route::middleware('api-user')->post('/load-avatar', 'UserController@loadImages');
    Route::middleware('api-user')->get('/get-user', 'UserController@getUser');
    Route::middleware('api-user')->post('/create', 'UserController@createFromAdmin');
});

Route::group(['prefix' => 'shops', 'middleware' => 'api', 'namespace' => 'Shops'], function () {
    Route::middleware('api-user')->post('/create', 'UserShopsController@create');
    Route::middleware('api-user')->delete('/delete/{id}', 'UserShopsController@delete');
    Route::get('/', 'UserShopsController@listShops');
});

Route::group(['prefix' => 'categories', 'middleware' => 'api', 'namespace' => 'Category'], function () {
    Route::get('/{category}', 'CategoryController@getCategory');
    Route::get('/', 'CategoryController@listCategories');
    Route::post('/create', 'CategoryController@create');
    Route::post('/filters', 'CategoryController@filterListCategories');
    Route::middleware('api-user')->delete('/delete/{id}', 'CategoryController@delete');
});

Route::group(['prefix' => 'products', 'middleware' => 'api', 'namespace' => 'Products'], function () {
    Route::middleware('api-user')->post('/create', 'ProductsController@create');
    Route::get('/', 'ProductsController@listProducts');
    Route::post('/image-upload', 'ProductsController@loadImages');
    Route::middleware('api-user')->patch('/{id}/update', 'ProductsController@update');
});

