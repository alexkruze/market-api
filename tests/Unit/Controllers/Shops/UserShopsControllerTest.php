<?php

namespace Tests\Unit\Controllers\Shops;

use App\Models\User;
use App\Models\UserShop;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserShopsControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testFailCreate()
    {
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        $request = [];

        $response = $this->post('/api/shops/create', $request, ['Authorization' => 'Bearer ' . $token]);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 0,
                'errors' => []
            ]);
    }

    public function testSuccessCreate()
    {
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        $request = factory(UserShop::class)->make()->toArray();

        $response = $this->post('/api/shops/create', $request, ['Authorization' => 'Bearer ' . $token]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'status' => 1,
            ]);
        UserShop::where('slug', str_slug($request['shop_name']))->delete();
    }

    public function testDelete()
    {
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        $request = factory(UserShop::class)->make()->toArray();

        $response_shop = $this->post('/api/shops/create', $request, ['Authorization' => 'Bearer ' . $token]);
        $response_shop = json_decode($response_shop->content());
        if ($response_shop->status) {
            $shop = UserShop::where('slug', str_slug($request['shop_name']))->first();
            $response = $this->delete('/api/shops/delete/' . $shop->id, [], ['Authorization' => 'Bearer ' . $token]);
            $response
                ->assertStatus(200)
                ->assertJson([
                    'status' => 1,
                ]);
        }
    }

    public function testListShops()
    {
        $response = $this->get('/api/shops');
        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 1,
                'data' => []
            ]);
    }
}
