<?php

namespace Tests\Unit\Controllers\User;

use App\Models\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request as RequestData;

class UserControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetUser()
    {
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        $response = $this->get('/api/users/get-user', ['Authorization' => 'Bearer ' . $token]);

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'status' => 1,
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'avatar' => $user->avatar

                ]
            ]);
    }

    public function testListUsers()
    {
        $response = $this->get('/api/users');
        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 1,
                'users' => []
            ]);
    }

    public function testFailCreateFromAdmin()
    {
        $request = new RequestData();
        $user = User::first();
        $token = JWTAuth::fromUser($user);

        $response = $this->post('/api/users/create', $request->all(), ['Authorization' => 'Bearer ' . $token]);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 0,
                'errors' => []
            ]);
    }

    public function testSuccessCreateFromAdmin()
    {
        $request = factory(User::class)->make()->makeHidden('avatar')->toArray();
        $user = User::first();
        $token = JWTAuth::fromUser($user);

        $response = $this->post('/api/users/create', $request, ['Authorization' => 'Bearer ' . $token]);
        $response
            ->assertStatus(202)
            ->assertJson([
                'status' => 1,
            ]);
        User::where('email', $request['email'])->delete();
    }

    public function testUpdate()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);
        $fake_data = [
            'name' => 'lorem',
            'full_name' => 'Ipsum Petrovich'
        ];

        $response = $this->patch('/api/users', $fake_data, ['Authorization' => 'Bearer ' . $token]);
        $response
            ->assertStatus(202)
            ->assertJson([
                'status' => 1,
            ]);
        User::where('email', $user->email)->delete();
    }

    public function testLoadImages()
    {
        $file = UploadedFile::fake()->image('avatar.jpg');
        $user = User::first();
        $token = JWTAuth::fromUser($user);

        $response = $this->post('/api/users/load-avatar', ['image' => $file], ['Authorization' => 'Bearer ' . $token]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'status' => 1,
            ]);
        $file_response = json_decode($response->content());
        if ($file_response->status) {
            Storage::disk('public')->delete('tmp/' . $file_response->name);
        }
    }
}
